﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GraphicsAssignmentThree
{
    class SkyBox
    {
        GraphicsDeviceManager graphics;
        // The basicEffect class controls how a 3D object is rendered by affecting it lighting, culling and camera control
        BasicEffect effect;
        IServiceProvider game1Service;

        Model skybox;
        Matrix[] boneTransforms_sky;

        // Textures for cube and sky box
        Texture2D[] sky_textures = new Texture2D[6];

        // Various objects need to be passed into this class to creat the skybox  in Game1.cs
        public SkyBox(GraphicsDeviceManager graphics, IServiceProvider game1Service)
        {
            this.graphics = graphics;
            this.game1Service = game1Service;
        }

        public void LoadSkyBox()
        {
            ContentManager contentLoader = new ContentManager(game1Service);
            effect = new BasicEffect(this.graphics.GraphicsDevice, null);
            effect.TextureEnabled = true;
            skybox = contentLoader.Load<Model>(@"Content\Models\SkyBox\skybox2");
            boneTransforms_sky = new Matrix[skybox.Bones.Count];
            skybox.CopyAbsoluteBoneTransformsTo(boneTransforms_sky);
        }

        public void DrawSkyBox(GameTime gameTime, Matrix view, Matrix projection)
        {
            skybox.CopyAbsoluteBoneTransformsTo(boneTransforms_sky);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    // Change made to the defualt lighting!
                    effect.AmbientLightColor = new Vector3(0.5f, 0.5f, 0.5f);
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = boneTransforms_sky[mesh.ParentBone.Index];
                }
                mesh.Draw();
            }       
        }
    }
}