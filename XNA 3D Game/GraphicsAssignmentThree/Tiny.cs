﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace GraphicsAssignmentThree
{
    
    class Tiny
    {
        GraphicsDeviceManager graphics;
        IServiceProvider game1Service;


        // Data structures required to store Tiny
        Model tiny;
        Matrix[] tiny_boneTransforms;
        Matrix tiny_transform;

        // Tiny position (Camera located above and behind Tiny)
        Vector3 tinyPosition = new Vector3(0, 0, 0);
        Quaternion tinyRotation = Quaternion.Identity;
        float dx, dy, dz;

        // Tiny Constructor
        public Tiny(GraphicsDeviceManager graphics, IServiceProvider game1Service)
        {
            this.graphics = graphics;
            this.game1Service = game1Service;
        }

        public void LoadTiny()
        {
            ContentManager contentLoader = new ContentManager(game1Service);


            // Load model from run-time directory (debug/release)
            tiny = contentLoader.Load<Model>(@"Content\Models\Tiny\tiny");
            tiny_boneTransforms = new Matrix[tiny.Bones.Count];
            tiny.CopyAbsoluteBoneTransformsTo(tiny_boneTransforms);
            tiny_transform = tiny.Root.Transform * Matrix.CreateScale(0.001f); // 0.05 Campus

            tiny_transform = tiny.Root.Transform * Matrix.CreateScale(0.003f) * Matrix.CreateRotationX(MathHelper.PiOver2) * Matrix.CreateRotationY(MathHelper.Pi);
        }

        public void DrawTiny(GameTime gameTime, Matrix view, Matrix projection)
        {
            tiny.CopyAbsoluteBoneTransformsTo(tiny_boneTransforms);
            // Resets the position of Tiny to her original position evey time a frame is drawn.
            // tiny.Root.Transform = tiny_transform;
            
            // Draws Tiny at her new location in every frame.
            tiny.Root.Transform = tiny_transform * Matrix.CreateFromQuaternion(tinyRotation) * Matrix.CreateTranslation(tinyPosition); 
            
            
            foreach (ModelMesh mesh in tiny.Meshes)
            {
                // The basicEffect class controls how a 3D object is rendered by affecting it lighting, culling and camera control
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = tiny_boneTransforms[mesh.ParentBone.Index];
                }
                mesh.Draw();
            }

           
        }

        public void UpdateTiny(KeyboardState keys, Matrix view, Matrix projection)
        {/*
            //  Camera relative to Tiny
            Vector3 campos = Vector3.Transform(new Vector3(0, 1.5f, -3.5f), Matrix.CreateFromQuaternion(tiny.ptinyRotation));
            // Movement ofthe camera with Tiny. With a + it follows her, with a - it move towards her then away.
            campos += tiny.ptinyPosition;

            Vector3 camup = new Vector3(0, 1, 0);
            camup = Vector3.Transform(camup, Matrix.CreateFromQuaternion(tiny.ptinyRotation));

            view = Matrix.CreateLookAt(campos, tiny.ptinyPosition, camup);
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, graphics.GraphicsDevice.Viewport.AspectRatio, 0.2f, 500.0f);

            #region // Tiny update

            Vector3 changepos = Vector3.Transform(Vector3.Forward, Matrix.CreateFromQuaternion(tiny.ptinyRotation));
            tiny.ptinyPosition -= Vector3.Multiply(changepos, 0.05f);

            if (keys.IsKeyDown(Keys.Space))
            {
                tiny.ptinyPosition = new Vector3(0, 2, -24);
                tiny.ptinyRotation = Quaternion.Identity;
            }

            // Reset yaw, pitch and roll each time
            tiny.pdx = 0f; tiny.pdy = 0f; tiny.pdz = 0f;
            // Yaw
            if (keys.IsKeyDown(Keys.Right)) tiny.pdy = -0.01f;
            if (keys.IsKeyDown(Keys.Left)) tiny.pdy = 0.01f;
            // Pitch
            if (keys.IsKeyDown(Keys.Down)) tiny.pdx = -0.01f;
            if (keys.IsKeyDown(Keys.Up)) tiny.pdx = 0.01f;
            // Roll 
            if (keys.IsKeyDown(Keys.OemComma)) tiny.pdz = 0.01f;
            if (keys.IsKeyDown(Keys.OemPeriod)) tiny.pdz = -0.01f;

            tiny.ptinyRotation *= Quaternion.CreateFromAxisAngle(Vector3.Up, tiny.pdy) * Quaternion.CreateFromAxisAngle(Vector3.Right, tiny.pdx) * Quaternion.CreateFromAxisAngle(Vector3.Backward, tiny.pdz);
            #endregion
          */
        }

        #region //Getter and Setter methods
        public Vector3 ptinyPosition { get {return tinyPosition;} set{ tinyPosition = value;} }
        public Quaternion ptinyRotation { get { return tinyRotation; } set { tinyRotation = value; } }
        public float pdx { get { return dx; } set { dx = value;} }
        public float pdy { get { return dy; } set { dy = value; } }
        public float pdz { get { return dz; } set { dz = value; } }
        #endregion
       



    }
}
